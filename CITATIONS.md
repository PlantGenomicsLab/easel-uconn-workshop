# PlantGenomicsLab/easel: Citations

## [Nextflow](https://pubmed.ncbi.nlm.nih.gov/28398311/)

> Di Tommaso P, Chatzou M, Floden EW, Barja PP, Palumbo E, Notredame C. Nextflow enables reproducible computational workflows. Nat Biotechnol. 2017 Apr 11;35(4):316-319. doi: 10.1038/nbt.3820. PubMed PMID: 28398311.

## [nf-core](https://pubmed.ncbi.nlm.nih.gov/32055031/)

> Ewels PA, Peltzer A, Fillinger S, Patel H, Alneberg J, Wilm A, Garcia MU, Di Tommaso P, Nahnsen S. The nf-core framework for community-curated bioinformatics pipelines. Nat Biotechnol. 2020 Mar;38(3):276-278. doi: 10.1038/s41587-020-0439-x. PubMed PMID: 32055031.

## Pipeline tools

- [EnTAP](https://entap.readthedocs.io/en/latest/)
  > Hart AJ, Ginzburg S, Xu M, et al. EnTAP: Bringing faster and smarter functional annotation to non-model eukaryotic transcriptomes. Mol Ecol Resour. 2020;20:591–604. https://doi.org/10.1111/1755-0998.13106
- [Diamond](https://github.com/bbuchfink/diamond)

  > Buchfink B, Xie C, Huson DH. "Fast and sensitive protein alignment using DIAMOND", Nature Methods 12, 59-60 (2015). doi:10.1038/nmeth.3176

- [gFACS](https://gfacs.readthedocs.io/en/latest/)
  > Caballero, M., & Wegrzyn, J. (2019). gFACs: Gene Filtering, Analysis, and Conversion to Unify Genome Annotations Across Alignment and Gene Prediction Frameworks. Genomics, Proteomics & Bioinformatics, 17(3), 305–310.

- [EggNOG-mapper](https://github.com/eggnogdb/eggnog-mapper)
  > Cantalapiedra, C. P., Hernández-Plaza, A., Letunic, I., Bork, P., & Huerta-Cepas, J. (2021). eggNOG-mapper v2: Functional Annotation, Orthology    Assignments, and Domain Prediction at the Metagenomic Scale. Molecular Biology and Evolution, 38(12), 5825–5829.

- [fastp](https://github.com/OpenGene/fastp)
  > Chen, S., Zhou, Y., Chen, Y., & Gu, J. (2018). fastp: an ultra-fast all-in-one FASTQ preprocessor. Bioinformatics , 34(17), i884–i890.

- [AGAT](https://github.com/NBISweden/AGAT)
  > Dainat, J., Hereñú, D., Davis, E., Crouch, K., LucileSol, Agostinho, N., pascal-git, & tayyrov. (2022). NBISweden/AGAT: AGAT-v1.0.0. https://doi.org/10.5281/zenodo.7255559

- [GenomeThreader](https://genomethreader.org/)
  > Gremme, G., Brendel, V., Sparks, M. E., & Kurtz, S. (2005). Engineering a software tool for gene structure prediction in higher organisms. Information and Software Technology, 47(15), 965–978.

- [BLAST](https://blast.ncbi.nlm.nih.gov/)
  > Johnson, M., Zaretskaya, I., Raytselis, Y., Merezhuk, Y., McGinnis, S., & Madden, T. L. (2008). NCBI BLAST: a better web interface. Nucleic Acids Research, 36(suppl_2), W5–W9.

- [HISAT2](http://daehwankimlab.github.io/hisat2/)
  > Kim, D., Paggi, J. M., Park, C., Bennett, C., & Salzberg, S. L. (2019). Graph-based genome alignment and genotyping with HISAT2 and HISAT-genotype. Nature Biotechnology, 37(8), 907–915.

- [seqtk](https://github.com/lh3/seqtk)
  > Li, H. (n.d.). seqtk: Toolkit for processing sequences in FASTA/Q formats. Github. Retrieved January 30, 2023, from https://github.com/lh3/seqtk

- [miniprot](https://github.com/lh3/miniprot)
  > Li, H. (2023). Protein-to-genome alignment with miniprot. Bioinformatics , 39(1). https://doi.org/10.1093/bioinformatics/btad014

- [ViennRNA](https://github.com/ViennaRNA/ViennaRNA)
  > Lorenz, R., Bernhart, S. H., Höner Zu Siederdissen, C., Tafer, H., Flamm, C., Stadler, P. F., & Hofacker, I. L. (2011). ViennaRNA Package 2.0. Algorithms for Molecular Biology: AMB, 6, 26.

- [BUSCO](https://busco.ezlab.org/)
  > Manni, M., Berkeley, M. R., Seppey, M., Simão, F. A., & Zdobnov, E. M. (2021). BUSCO Update: Novel and Streamlined Workflows along with Broader and Deeper Phylogenetic Coverage for Scoring of Eukaryotic, Prokaryotic, and Viral Genomes. Molecular Biology and Evolution, 38(10), 4647–4654.

- [GFFRead/GFFCompare](http://ccb.jhu.edu/software/stringtie/gff.shtml)
  > Pertea, G., & Pertea, M. (2020). GFF utilities: GffRead and GffCompare. F1000Research, 9, 304.

- [VSEARCH](https://github.com/torognes/vsearch)
  > Rognes, T., Flouri, T., Nichols, B., Quince, C., & Mahé, F. (2016). VSEARCH: a versatile open source tool for metagenomics. PeerJ, 4, e2584.

- [SeqKit](https://bioinf.shenwei.me/seqkit/)
  > Shen, W., Le, S., Li, Y., & Hu, F. (2016). SeqKit: A Cross-Platform and Ultrafast Toolkit for FASTA/Q File Manipulation. PloS One, 11(10), e0163962.

- [StringTie](https://ccb.jhu.edu/software/stringtie/)
  > Shumate, A., Wong, B., Pertea, G., & Pertea, M. (2022). Improved transcriptome assembly using a hybrid of long and short reads with StringTie. PLoS Computational Biology, 18(6), e1009730.

- [PsiCLASS](https://github.com/splicebox/PsiCLASS)
  > Song, L., Sabunciyan, S., Yang, G., & Florea, L. (2019). A multi-sample approach increases the accuracy of transcript assembly. Nature Communications, 10(1), 5000.

- [AUGUSTUS](https://github.com/Gaius-Augustus/Augustus)
  > Stanke, M., Keller, O., Gunduz, I., Hayes, A., Waack, S., & Morgenstern, B. (2006). AUGUSTUS: ab initio prediction of alternative transcripts. Nucleic Acids Research, 34(Web Server issue), W435–W439.

- [TransDecoder](https://github.com/TransDecoder/TransDecoder)
  > TransDecoder: TransDecoder source. (n.d.). Github. Retrieved January 30, 2023, from https://github.com/TransDecoder/TransDecoder

- [GMAP](https://academic.oup.com/bioinformatics/article/21/9/1859/409207)
  > Wu, T. D., & Watanabe, C. K. (2005). GMAP: a genomic mapping and alignment program for mRNA and EST sequences. Bioinformatics , 21(9), 1859–1875.

## Software packaging/containerisation tools

- [Anaconda](https://anaconda.com)

  > Anaconda Software Distribution. Computer software. Vers. 2-2.4.0. Anaconda, Nov. 2016. Web.

- [Bioconda](https://pubmed.ncbi.nlm.nih.gov/29967506/)

  > Grüning B, Dale R, Sjödin A, Chapman BA, Rowe J, Tomkins-Tinch CH, Valieris R, Köster J; Bioconda Team. Bioconda: sustainable and comprehensive software distribution for the life sciences. Nat Methods. 2018 Jul;15(7):475-476. doi: 10.1038/s41592-018-0046-7. PubMed PMID: 29967506.

- [BioContainers](https://pubmed.ncbi.nlm.nih.gov/28379341/)

  > da Veiga Leprevost F, Grüning B, Aflitos SA, Röst HL, Uszkoreit J, Barsnes H, Vaudel M, Moreno P, Gatto L, Weber J, Bai M, Jimenez RC, Sachsenberg T, Pfeuffer J, Alvarez RV, Griss J, Nesvizhskii AI, Perez-Riverol Y. BioContainers: an open-source and community-driven framework for software standardization. Bioinformatics. 2017 Aug 15;33(16):2580-2582. doi: 10.1093/bioinformatics/btx192. PubMed PMID: 28379341; PubMed Central PMCID: PMC5870671.

- [Docker](https://dl.acm.org/doi/10.5555/2600239.2600241)

- [Singularity](https://pubmed.ncbi.nlm.nih.gov/28494014/)
  > Kurtzer GM, Sochat V, Bauer MW. Singularity: Scientific containers for mobility of compute. PLoS One. 2017 May 11;12(5):e0177459. doi: 10.1371/journal.pone.0177459. eCollection 2017. PubMed PMID: 28494014; PubMed Central PMCID: PMC5426675.
