include { psiclass; psiclass_fasta; orf_psiclass; eggnog_psiclass; predict_psiclass; genemodel_psiclass; cluster_psiclass; getComplete_psiclass } from '../modules/psiclass_gene_model.nf'
include { ESThints_psiclass; miniprot_psiclass; proteinHints_psiclass; gffread_gtf; trainingSet_psiclass; augustusEST_psiclass; augustusProtein_psiclass; combineEST_psiclass; combineProtein_psiclass; combineAll_psiclass } from '../modules/psiclass_prediction.nf'

workflow assembly_psiclass {
    take:
    bam
	script
	bin
	genome
	split
	augustus_config
	configEST
	configProtein
	eggnog_db

    main:
	list_ch = bam.toList()
	psiclass	                ( list_ch, params.psiclass )
	psiclass_fasta				( psiclass.out.gtf, genome )
    orf_psiclass         	    ( psiclass_fasta.out.fasta )
	eggnog_psiclass  	        ( orf_psiclass.out.longOrfsPepFiles, eggnog_db )
	predict_psiclass 	        ( psiclass_fasta.out.fasta, eggnog_psiclass.out.eggnogBlastp, orf_psiclass.out.LongOrfsDirFiles )
	genemodel_psiclass          ( predict_psiclass.out.transdecoderGFF3, psiclass_fasta.out.gff3, psiclass_fasta.out.fasta )
	cluster_psiclass 	        ( predict_psiclass.out.transdecoderCDS, params.cluster_id )
    getComplete_psiclass 	    ( cluster_psiclass.out.centroids, genemodel_psiclass.out.genomeGFF3 )
    ESThints_psiclass 	        ( getComplete_psiclass.out.GeneModel )
	miniprot_psiclass			( genome, predict_psiclass.out.transdecoderPEP, params.miniprot_N, params.miniprot_outn, params.miniprot_max_intronlen )
	proteinHints_psiclass	    ( miniprot_psiclass.out.psiclass_miniprot )
	genome_ch = split.flatten().map { file -> tuple(file.baseName, file) }
	gffread_gtf					( getComplete_psiclass.out.GeneModel )
	trainingSet_psiclass	    ( gffread_gtf.out.gtf, getComplete_psiclass.out.GeneModel, genome, augustus_config, script, bin, params.prefix, params.test, params.train )
	augustusEST_psiclass 	    ( trainingSet_psiclass.out.gb, ESThints_psiclass.out.psiclass_est, script, configEST, augustus_config, bin, genome_ch, params.prefix )
	augustusProtein_psiclass    ( trainingSet_psiclass.out.gb, proteinHints_psiclass.out.psiclass_protein, script, configProtein, augustus_config, bin, genome_ch, params.prefix )
	est_ch = augustusEST_psiclass.out.psiclass_estHints.collect()
	combineEST_psiclass         ( est_ch )
	protein_ch = augustusProtein_psiclass.out.psiclass_proteinHints.collect()
	combineProtein_psiclass     ( protein_ch )
	combineAll_psiclass         ( combineProtein_psiclass.out.psiclass_proteinHints.collect(), combineEST_psiclass.out.estGFF.collect() )

	emit:
	combineAll_psiclass.out
	combineEST_psiclass.out
	combineProtein_psiclass.out
	genemodel_psiclass.out
	psiclass_fasta.out.log_psiclass
	trainingSet_psiclass.out
	
}
