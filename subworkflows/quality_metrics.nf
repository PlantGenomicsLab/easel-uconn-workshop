include { busco_unfiltered; busco_filtered; agat_unfiltered; agat_filtered; entap_config; entap_unfiltered; entap_filtered; unfiltered_output; filtered_output } from '../modules/metrics.nf'

workflow summary_stats {

    take:
    protein
    odb
    unfiltered_gff
    filtered_gff
    protein_filtered
    data_eggnog
    entap_db
    eggnog_db
    
    main:
	busco_unfiltered         ( protein, odb )
    agat_unfiltered         ( unfiltered_gff, params.prefix )
    entap_config             ( params.taxon, params.tcoverage, params.qcoverage, entap_db, eggnog_db, data_eggnog )
    entap_unfiltered         ( protein, entap_config.out.config, params.reference_db )
    unfiltered_output        ( busco_unfiltered.out.busco_unfiltered_txt, agat_unfiltered.out.unfiltered_stats, entap_unfiltered.out.entap_unfiltered_log)

    busco_filtered           ( protein_filtered, odb )
    agat_filtered           ( filtered_gff, params.prefix )
    entap_filtered           ( protein_filtered, entap_config.out.config, params.reference_db )
    filtered_output          ( busco_filtered.out.busco_filtered_txt, agat_filtered.out.filtered_stats, entap_filtered.out.entap_filtered_log )

    emit:
    unfiltered_output.out
    filtered_output.out
}
