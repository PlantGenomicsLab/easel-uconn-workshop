include { merge_gff; fix_exons; format_gff; list } from '../modules/combine_predictions.nf'
include { gtf } from '../modules/filtering/tasks_gff_to_gtf.nf'
include { htseq; gene_expression } from '../modules/filtering/tasks_gene_expression.nf'
include { orthodb_bed; orthodb_feature } from '../modules/filtering/tasks_orthodb.nf'
include { support } from '../modules/filtering/tasks_support.nf'
include { mono_multi } from '../modules/filtering/tasks_multi_mono.nf' 
include { unfiltered_protein; eggnog; eggnog_feature } from '../modules/filtering/tasks_gene_family.nf' 
include { start_stop_codons } from '../modules/filtering/tasks_full_length.nf' 
include { cds_extract; cds_repeat_content } from '../modules/filtering/tasks_repeat_cds.nf' 
include { map } from '../modules/filtering/tasks_map.nf' 
include { transcript_extract; length; molecular_weight; gc; gc_python } from '../modules/filtering/tasks_transcript.nf'
include { true_start_site; pad; split_fasta; rna_fold; merge; matrix } from '../modules/filtering/tasks_folding.nf' 
include { regressor; classifier; filtered; filtered_protein } from '../modules/filtering/tasks_random_forest.nf'

workflow filter {

    take:
    stringtie2_combined
    psiclass_combined
    genome
    s_est
    s_prot
    p_est
    p_prot
    eggnog_db
    bam
    orthodb_align


    main:
    merge_gff		        ( stringtie2_combined.collect(), psiclass_combined.collect() )
    fix_exons		        ( merge_gff.out.unfiltered )
    format_gff              ( fix_exons.out.fixed, genome, params.prefix)
    list                    ( s_est, s_prot, p_est, p_prot )       
    gtf                     ( format_gff.out.unfiltered_fixed, params.prefix )
    htseq                   ( bam, gtf.out.unfiltered_gtf )
    orthodb_bed             ( orthodb_align, format_gff.out.unfiltered_fixed )
    support                 ( format_gff.out.unfiltered_fixed, list.out.gff )
    orthodb_feature         ( support.out.support, orthodb_bed.out.bed )
    gene_expression         ( htseq.out.counts.collect(), support.out.support)
    mono_multi              ( format_gff.out.unfiltered_fixed, support.out.support )
    unfiltered_protein      ( genome, gtf.out.unfiltered_gtf, params.prefix )
    eggnog                  ( unfiltered_protein.out.protein, eggnog_db )
    eggnog_feature          ( support.out.support, eggnog.out.mapper )
    start_stop_codons       ( format_gff.out.unfiltered_fixed, support.out.support )
    cds_extract             ( genome, gtf.out.unfiltered_gtf, support.out.support )
    cds_repeat_content      ( cds_extract.out.cds, support.out.support )
    transcript_extract      ( genome, gtf.out.unfiltered_gtf, support.out.support )
    length                  ( transcript_extract.out.transcript, support.out.support )
    molecular_weight        ( transcript_extract.out.transcript, support.out.support )
    gc                      ( transcript_extract.out.transcript )
    gc_python               ( support.out.support, gc.out.gc )
    true_start_site         ( format_gff.out.unfiltered_fixed )
    pad                     ( params.window, true_start_site.out.text, genome )
    split_fasta             ( pad.out.fasta, params.parts )
    fasta_ch = split_fasta.out.split.flatten().map { file -> tuple(file.baseName, file) }
    rna_fold                ( fasta_ch )
    merge                   ( rna_fold.out.rna_fold.collect() )
	matrix                  ( params.window, support.out.support, merge.out.merge_rna )
    map                     ( matrix.out.gc_ratio.collect(), matrix.out.free_energy.collect(), gc_python.out.gc_track.collect(), length.out.length.collect(), molecular_weight.out.weight.collect(), support.out.support.collect(), mono_multi.out.exonic.collect(), eggnog_feature.out.eggnog.collect(), start_stop_codons.out.start_codon.collect(), start_stop_codons.out.stop_codon.collect(), cds_repeat_content.out.cds_repeat.collect(), orthodb_feature.out.orthodb.collect(), gene_expression.out.expression.collect())
    regressor               ( map.out.features, params.training_set )
    classifier              ( map.out.features, params.training_set )
    filtered                ( classifier.out.c_rf, regressor.out.r_rf, gtf.out.unfiltered_gtf, params.prefix, params.regressor )
    filtered_protein        ( filtered.out.filtered_prediction, genome, params.prefix )

    emit:
    fix_exons.out
    unfiltered_protein.out.protein
    filtered_protein.out
    filtered.out.filtered_gff
}
