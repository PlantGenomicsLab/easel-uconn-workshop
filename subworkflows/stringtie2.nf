include { stringtie2; merge_stringtie2; stringtie2_fasta; orf_stringtie2; eggnog_stringtie2; predict_stringtie2; genemodel_stringtie2; cluster_stringtie2; getComplete_stringtie2 } from '../modules/stringtie2_gene_model.nf'
include { ESThints_stringtie2; miniprot_stringtie2; proteinHints_stringtie2; trainingSet_stringtie2; augustusEST_stringtie2; gffread_gtf; augustusProtein_stringtie2; combineEST_stringtie2; combineProtein_stringtie2; combineAll_stringtie2 } from '../modules/stringtie2_prediction.nf'

workflow assembly_stringtie2 {
    take:
	hisat_alignments
	script
	bin
	genome
	split
	augustus_config
	configEST
	configProtein
	eggnog_db

    main:
	
	stringtie2	                ( hisat_alignments, params.gap_tolerance )
	list_ch = stringtie2.out.stringtie2_gtf.collect()
	merge_stringtie2            ( list_ch )
	stringtie2_fasta			( genome, merge_stringtie2.out.merged_gtf )
    orf_stringtie2         	    ( stringtie2_fasta.out.fasta )
    eggnog_stringtie2           ( orf_stringtie2.out.longOrfsPepFiles, eggnog_db )
    predict_stringtie2 	        ( stringtie2_fasta.out.fasta, eggnog_stringtie2.out.eggnogBlastp, orf_stringtie2.out.LongOrfsDirFiles )
	genemodel_stringtie2        ( predict_stringtie2.out.transdecoderGFF3, stringtie2_fasta.out.gff3, stringtie2_fasta.out.fasta )
	cluster_stringtie2 	        ( predict_stringtie2.out.transdecoderCDS, params.cluster_id )
    getComplete_stringtie2 	    ( cluster_stringtie2.out.centroids, genemodel_stringtie2.out.genomeGFF3 )
	ESThints_stringtie2 	    ( getComplete_stringtie2.out.GeneModel )
	miniprot_stringtie2			( genome, predict_stringtie2.out.transdecoderPEP, params.miniprot_N, params.miniprot_outn, params.miniprot_max_intronlen )
	proteinHints_stringtie2	    ( miniprot_stringtie2.out.stringtie2_miniprot )
	genome_ch = split.flatten().map { file -> tuple(file.baseName, file) }
	gffread_gtf					( getComplete_stringtie2.out.GeneModel )
	trainingSet_stringtie2		( gffread_gtf.out.gtf,  getComplete_stringtie2.out.GeneModel, genome, augustus_config, script, bin, params.prefix, params.test, params.train )
	augustusEST_stringtie2 	    ( trainingSet_stringtie2.out.gb, ESThints_stringtie2.out.stringtie2_est, script, configEST, augustus_config, bin, genome_ch, params.prefix )
	augustusProtein_stringtie2  ( trainingSet_stringtie2.out.gb, proteinHints_stringtie2.out.stringtie2_protein, script, configProtein, augustus_config, bin, genome_ch, params.prefix )
	est_ch = augustusEST_stringtie2.out.stringtie2_estHints.collect()
	combineEST_stringtie2       ( est_ch )
	protein_ch = augustusProtein_stringtie2.out.stringtie2_proteinHints.collect()
	combineProtein_stringtie2   ( protein_ch )
	combineAll_stringtie2       ( combineProtein_stringtie2.out.stringtie_proteinHints.collect(), combineEST_stringtie2.out.estGFF.collect() )

    emit:
	combineAll_stringtie2.out
	combineEST_stringtie2.out
	combineProtein_stringtie2.out
	genemodel_stringtie2.out
	stringtie2_fasta.out.log_stringtie
	trainingSet_stringtie2.out
}