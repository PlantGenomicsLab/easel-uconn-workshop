include { proteinHints_orthodb; augustusProtein_psiclass; augustusProtein_stringtie2; combineProtein_stringtie2; combineProtein_psiclass  } from '../modules/extrinsic_evidence.nf'

workflow orthodb_hints {
   
	take:
	script
	bin
	genome
	split
	augustus_config
	configEST
	configProtein
	psiclass_train
	stringtie2_train
	orthodb
	
    main:
	genome_ch = split.flatten().map { file -> tuple(file.baseName, file) }
    proteinHints_orthodb            ( orthodb )
    augustusProtein_psiclass        ( psiclass_train, proteinHints_orthodb.out.protein, script, configProtein, augustus_config, bin, genome_ch, params.prefix )
    psiclass_ch = augustusProtein_psiclass.out.psiclass_proteinHints.collect()
	combineProtein_psiclass     	( psiclass_ch )
	augustusProtein_stringtie2      ( stringtie2_train, proteinHints_orthodb.out.protein, script, configProtein, augustus_config, bin, genome_ch, params.prefix )
	stringtie_ch = augustusProtein_stringtie2.out.stringtie2_proteinHints.collect()
	combineProtein_stringtie2     	( stringtie_ch )
	
	emit:
	combineProtein_psiclass.out
	combineProtein_stringtie2.out

}