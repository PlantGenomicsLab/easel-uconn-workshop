include { percent_masked; split_genome; fold_genome } from '../modules/split_genome.nf'
include { fetch_rna; fastp; remove_low_qc; log_qc } from '../modules/rna_reads.nf'
include { orthodb_align; hisat2_index; hisat2_align; removing_samples; log_mr } from '../modules/alignments.nf'
include { entap; orthodb } from '../modules/databases.nf'


workflow data_input {

    take:
    genome
    scripts
	
    main:
    entap               ( params.config_ini )
    orthodb             ( params.order, params.orthoDB )
    percent_masked      ( genome )
	split_genome		( genome, scripts, params.bins )
    fold_genome         ( genome, params.prefix )
    if(params.user_reads == 'false'){
    sra_ch = Channel.fromPath(params.sra).splitCsv().flatten()
    fetch_rna           ( sra_ch )
    fastp               ( fetch_rna.out.sra )
    }
    else if(params.sra == 'false'){
    read_pairs_ch = Channel.fromFilePairs(params.user_reads)
    fastp               ( read_pairs_ch )
    }
    remove_low_qc       ( fastp.out.trimmed_json, params.total_reads, params.mean_length )
    log_qc              ( remove_low_qc.out.qc.collect(), params.total_reads, params.mean_length )
    orthodb_align       ( genome, orthodb.out.protein, params.miniprot_N, params.miniprot_outn, params.miniprot_max_intronlen )
    hisat2_index		( genome )
    hisat2_align		( remove_low_qc.out.pass_fastq, hisat2_index.out.hisat2index, params.hisat2_min_intronlen, params.hisat2_max_intronlen )
    removing_samples    ( hisat2_align.out.sam_tuple, params.rate )
    log_mr              ( removing_samples.out.mr.collect(), params.rate )

    emit:
	removing_samples.out.pass_bam_tuple
    removing_samples.out.pass_bam
    split_genome.out
    percent_masked.out
    log_qc.out
    log_mr.out
    entap.out.data_eggnog
    entap.out.entap_db
    entap.out.eggnog_db
    orthodb_align.out
    fold_genome.out
}
