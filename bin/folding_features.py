import re
import numpy as np
import linecache
import argparse
import os
import glob
import csv
import sys

dicts={}

cwd=sys.argv[1]
upstream=int(sys.argv[2])
downstream=int(sys.argv[3])

total_len=upstream+downstream
print(total_len)

for filename in glob.glob(cwd):
    print(filename)


    numSeqs = open(filename, 'r').read().count(">")
    print(numSeqs)

    f = open(filename, 'r')

    lines = f.readlines()
    sample=0

    for line in lines:
       i=lines.index(line)
       seq=lines[i+1]
       if line.startswith(">") == True:
           sample=sample+1
           print(sample)
       if line.startswith(">") == True and len(seq.strip()) == total_len: 
           #shannon=calcentropy(longer_kozak.upper())
           
           print(seq[upstream:upstream+3])
           struct=lines[i+2]
           print(struct[upstream:upstream+3])
           if len(struct.split(" "))>2:
                val=struct.split(" ")[2].strip()[:-1]
           elif len(struct.split(" "))==2:
                val=struct.split(" ")[1].strip()[1:-1]
           print(val)

           up_seq=seq[:upstream]
           g=seq[:upstream].upper().count("G")
           c=seq[:upstream].upper().count("C")
           gc_up=(g+c)/float(len(seq))
           g=seq[upstream:].upper().count("G")
           c=seq[upstream:].upper().count("C")
           gc_down=(g+c)/float(len(seq))
           gc_ratio=float(gc_up)/float(gc_down)


           string=str(val)+'\t'+str(gc_ratio)
           dicts[line.strip()[1:]]=string
           
           if numSeqs == sample:
               print('reached last sequence')
               break


#print(dicts)

a_file = open(sys.argv[4], "w")
writer = csv.writer(a_file, delimiter='\t')
for key, value in dicts.items():
    writer.writerow([key, value])

a_file.close()
