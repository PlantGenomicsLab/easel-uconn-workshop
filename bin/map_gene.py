import pandas as pd
import numpy as np
import sys

matrix = sys.argv[1]
feature = sys.argv[2]
header = sys.argv[3]
output = sys.argv[4]

df1 = pd.read_csv(matrix, delimiter='\t')
df2 = pd.read_csv(feature, delimiter='\t')

mapping = dict(df2[['Gene', 'Hit']].values)
df1[header] = df1.Gene.map(mapping)

df1.to_csv(output, sep='\t', encoding='utf-8', index=False)