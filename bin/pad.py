import argparse
import os
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq

parser = argparse.ArgumentParser(
     prog='pad.py',
     usage='''python pad.py --start_sites [gff file given by braker] --fasta [name of list file] --out [name of output fasta file] --up [number of basepairs upstream from the start site] --down [number of basepairs downstream from the start site]''',
     description='''This program pulls out the gff of specific genes, given the gff file and a list of sequences saved in a text file''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--start_sites', type=str, help='The name of the text file containing start sites', required=True)
parser.add_argument('--fasta', type=str, help='name of the list file that contains name of sequences', required=True)
parser.add_argument('--out', type=str, help='name of output fasta file', required=True)
parser.add_argument('--up', type=int, help='basepairs upstream from the start site', required=True)
parser.add_argument('--down', type=int, help='basepairs downstream from the start site', required=True)

args=parser.parse_args()
start_sites_file=args.start_sites
output=args.out
fastafile=args.fasta
downstream=args.down
upstream=args.up

id_dict=SeqIO.to_dict(SeqIO.parse(fastafile, "fasta"))
input_sites=open(start_sites_file, "r")
input_sites_read = input_sites.read()
lines = input_sites_read.split("\n") 
seqs={}
i=0
for line in lines:
    cols=line.split("\t")
    print (len(cols))
    if len(cols)==4:
        if cols[2]=="-":
            print('reverse')
            ups=int(cols[1])-1-downstream
            dws=int(cols[1])-1+upstream
            seq=str(id_dict[cols[0]].seq[ups:dws].reverse_complement())
        else:
            print('forward')
            ups=(int(cols[1])-1)-upstream
            dws=(int(cols[1])-1)+downstream
            seq=str(id_dict[cols[0]].seq[ups:dws])

        id_tag=cols[3]
        #if seq not in seqs.values():
        seqs[id_tag]=seq

    elif len(cols)==2:
        ups=(int(cols[1]))-upstream
        dws=(int(cols[1]))+downstream
        #ups=(int(cols[1])-1)-upstream
        #dws=(int(cols[1])-1)+downstream
        seq=str(id_dict[cols[0]].seq[ups:dws])
        print(seq)
        seqs[i]=seq
        i=i+1
    else:
        print('reached end of file')

with open (output, 'w') as o:
    for s in seqs.keys():
        o.write(">"+str(s)+"\n"+seqs.get(s)+"\n")
o.close()
