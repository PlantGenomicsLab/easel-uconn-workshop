# Import necessary modules
from Bio import SeqIO  # For parsing sequence files
import pandas as pd  # For creating and manipulating dataframes
import os  # For interacting with the operating system
import sys  # For accessing command-line arguments
import re  # For working with regular expressions
from Bio.SeqUtils import molecular_weight  # For calculating molecular weight of sequences

# Retrieve file path from command-line arguments
filepath = sys.argv[1]

# Parse the sequence file and store each sequence as a SeqRecord object
seq_objects = SeqIO.parse(filepath,'fasta')

# Create an empty list to store the SeqRecord objects
sequences = []

# Loop through each SeqRecord object and add it to the sequences list
for seq in seq_objects:
	sequences.append(seq)

# Create empty lists to store sequence information
seq_ids = []  # List to store sequence IDs
seq_lengths = []  # List to store sequence lengths
seq_genes = []  # List to store gene IDs
seq_weight = []  # List to store molecular weights

# Loop through each SeqRecord object and extract relevant information
for record in sequences:
    seq_id = record.id  # Get the sequence ID
    seq_description = record.description  # Get the sequence description
    fields = dict(re.findall(r'(\w+)=(.*?) ', seq_description))  # Extract gene ID using regex
    seq_gene = fields['gene']  # Get the gene ID
    sequence = record.seq  # Get the sequence
    length = len(sequence)  # Calculate the sequence length
    seq_genes.append(seq_gene)  # Add the gene ID to the seq_genes list
    seq_ids.append(seq_id)  # Add the sequence ID to the seq_ids list
    seq_lengths.append(length)  # Add the sequence length to the seq_lengths list
    try:
        weight = ("%0.2f" % molecular_weight(record.seq))  # Calculate the molecular weight of the sequence
        seq_weight.append(weight)  # Add the molecular weight to the seq_weight list
    except:
        seq_weight.append("NaN")  # If an error occurs while calculating the molecular weight, append "NaN" to seq_weight

# Create a pandas dataframe to store the sequence information
dataframe = pd.DataFrame()
dataframe['Transcript_ID'] = seq_ids
dataframe['Gene_ID'] = seq_genes
dataframe['Seq_Length'] = seq_lengths
dataframe['Molecular_Weight'] = seq_weight

# Save the dataframe to a tab-delimited text file
dataframe.to_csv('masked.tracking', sep='\t', encoding='utf-8', index=False)