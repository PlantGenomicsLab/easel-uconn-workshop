/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    VALIDATE INPUTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
// Check input path parameters to see if they exist
checkPathParamList = [
    params.genome
]
for (param in checkPathParamList) { if (param) { file(param, checkIfExists: true) } }
/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    IMPORT LOCAL MODULES/SUBWORKFLOWS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

include { data_input } from '../subworkflows/data_preparation.nf'
include { assembly_psiclass } from '../subworkflows/psiclass.nf'
include { assembly_stringtie2 } from '../subworkflows/stringtie2.nf'
include { orthodb_hints } from '../subworkflows/orthodb.nf'
include { filter } from '../subworkflows/filter_predictions.nf'
include { summary_stats } from '../subworkflows/quality_metrics.nf'
include { output } from '../subworkflows/log_easel.nf'
include { extrinsic_filter } from '../subworkflows/filter_extrinsic_evidence.nf'

workflow main_workflow {
	
	data_input 				( params.genome, params.scripts ) 
    assembly_stringtie2     ( data_input.out[0], params.scriptsAugustus, params.binAugustus, data_input.out[10], data_input.out[2], params.configAugustus, params.configEST, params.configProtein, data_input.out[6] )
	assembly_psiclass       ( data_input.out[1], params.scriptsAugustus, params.binAugustus, data_input.out[10], data_input.out[2], params.configAugustus, params.configEST, params.configProtein, data_input.out[6] )
	if(params.external_protein == 'true'){
	orthodb_hints			( params.scriptsAugustus, params.binAugustus, data_input.out[10], data_input.out[2], params.configAugustus, params.configEST, params.configProtein, assembly_psiclass.out[5], assembly_stringtie2.out[5], data_input.out[9])
	extrinsic_filter	 	( assembly_stringtie2.out[0], assembly_psiclass.out[0], data_input.out[10], assembly_stringtie2.out[1], assembly_stringtie2.out[2], assembly_psiclass.out[1], assembly_psiclass.out[2], orthodb_hints.out[0], orthodb_hints.out[1], data_input.out[7], data_input.out[0], data_input.out[9] )
	summary_stats		    ( extrinsic_filter.out[1], params.orthoDB, extrinsic_filter.out[0], extrinsic_filter.out[3], extrinsic_filter.out[2], data_input.out[6], data_input.out[7], data_input.out[8] )
	}
	else if(params.external_protein == 'false'){
	filter	 	( assembly_stringtie2.out[0], assembly_psiclass.out[0], data_input.out[10], assembly_stringtie2.out[1], assembly_stringtie2.out[2], assembly_psiclass.out[1], assembly_psiclass.out[2], data_input.out[6], data_input.out[0], data_input.out[9])
	summary_stats		    ( filter.out[1], params.orthoDB, filter.out[0], filter.out[3], filter.out[2], data_input.out[6], data_input.out[7], data_input.out[8] )
	}
	output					( data_input.out[3], data_input.out[4], data_input.out[5], assembly_stringtie2.out[4], assembly_psiclass.out[4], summary_stats.out[0], summary_stats.out[1] )
}

