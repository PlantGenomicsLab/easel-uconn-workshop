process psiclass {
    publishDir "$params.outdir/04_assembly/psiclass/gtf",  mode: 'copy', pattern: "*.gtf"
    tag { id }
    label 'process_medium'

   //     conda (params.enable_conda ? "bioconda::psiclass" : null)
   // if (workflow.containerEngine == 'singularity' && !params.singularity_pull_docker_container) {
   //     container "https://depot.galaxyproject.org/singularity/psiclass:1.0.3--h87f3376_0"
   // } else {
   //     container "quay.io/biocontainers/psiclass:1.0.3--h87f3376_0"
   // }
    container "sagnikbanerjee15/psiclass:1.0.3"
     
    input:
    path(bam)
    path(psiclass)
     
    output: 
    path("*gtf"), emit: all_gtf
    path("psiclass_vote.gtf"), emit: gtf
      
    """
    bam=\$(echo '${bam}' | sed -e "s/ /,/g") 
    psiclass -b \$bam -p ${task.cpus}
    """
}

process psiclass_fasta {
    publishDir "$params.outdir/04_assembly/psiclass",  mode: 'copy', pattern: "*.fa"
    publishDir "$params.outdir/log",  mode: 'copy', pattern: "*.txt"
    
        conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.0--pl5321hdfd78af_0' }"

    tag { id }
    label 'process_low'
    
    input:
    path(gtf)
    path(genome)
     
    output: 
    path("psiclass_transcripts.fa"), emit: fasta
    path("psiclass_transcripts.gff3"), emit: gff3  
    path("log_psiclass.txt"), emit: log_psiclass
      
    """
    ${projectDir}/bin/gtf_genome_to_cdna_fasta.pl ${gtf} ${genome} > psiclass_transcripts.fa 
    ${projectDir}/bin/gtf_to_alignment_gff3.pl ${gtf} > psiclass_transcripts.gff3

    transcripts=\$(echo | grep -c ">" psiclass_transcripts.fa)
    echo " " >> log_psiclass.txt
    echo "##### PsiCLASS Transcriptome Asssembly #####" >> log_psiclass.txt
    echo "\$transcripts transcripts" >> log_psiclass.txt
    """
}

process orf_psiclass {

    publishDir "$params.outdir/04_assembly/psiclass/frameselection",  mode: 'copy'
    label 'process_medium'

    tag { id }

    conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.0--pl5321hdfd78af_0' }"


    input:
    path(psiclassTranscriptome) 
     
    output:
    path "${psiclassTranscriptome}.transdecoder_dir/longest_orfs.pep", emit:longOrfsPepFiles
    path "${psiclassTranscriptome}.transdecoder_dir/", emit:LongOrfsDirFiles
    path "${psiclassTranscriptome}.transdecoder_dir/__checkpoints_longorfs/", emit:LongOrfsCheckpointsFiles
   //path "*.cmds", emit:longOrfsRootCmds
      
    """
    TransDecoder.LongOrfs -t ${psiclassTranscriptome}
    """
}

process eggnog_psiclass {
    publishDir "$params.outdir/04_assembly/psiclass/frameselection",  mode: 'copy'
    label 'process_medium' 

    conda "bioconda::eggnog-mapper"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/eggnog-mapper:2.1.9--pyhdfd78af_0' :
        'quay.io/biocontainers/eggnog-mapper:2.1.9--pyhdfd78af_0' }"
 
    
    input:
    path(psiclassPep)
    path(db)    

    output:
    path "*.hits", emit:eggnogBlastp 
   
    """
    emapper.py -i ${psiclassPep} --dmnd_db ${db} --no_annot -o eggnog.blastp -m diamond --cpu ${task.cpus}

    """
}

process predict_psiclass {
    publishDir "$params.outdir/04_assembly/psiclass/frameselection",  mode: 'copy'
    label 'process_medium'

    conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.0--pl5321hdfd78af_0' }"

    
    input:
    path(psiclassTranscriptomePredict) 
    path(eggnogBlastp)
    path(directory)
     
    output:
    path "${psiclassTranscriptomePredict}.transdecoder.pep", emit: transdecoderPEP
    path "${psiclassTranscriptomePredict}.transdecoder.bed", emit: transdecoderBED
    path "${psiclassTranscriptomePredict}.transdecoder.cds", emit: transdecoderCDS
    path "${psiclassTranscriptomePredict}.transdecoder.gff3", emit: transdecoderGFF3
      
    """
    TransDecoder.Predict -t ${psiclassTranscriptomePredict} --no_refine_starts --retain_blastp_hits ${eggnogBlastp}
    rm -r *.transdecoder_dir/__checkpoints_TDpredict/*
    """
}
process genemodel_psiclass {
    publishDir "$params.outdir/04_assembly/psiclass",  mode: 'copy'
    label 'process_medium'

    conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.0--pl5321hdfd78af_0' }"

    
    input:
    path(psiclassTranscriptomePredict) 
    path(gff3)
    path(transcript)
     
    output:
    path("psiclass.fasta.transdecoder.genome.gff3"), emit: genomeGFF3

      
    """
   ${projectDir}/bin/PerlLib/*.pm .
      ${projectDir}/bin/cdna_alignment_orf_to_genome_orf.pl \\
        ${psiclassTranscriptomePredict} \\
        ${gff3} \\
        ${transcript} > psiclass.fasta.transdecoder.genome.gff3
    """
}

process cluster_psiclass {
    publishDir "$params.outdir/04_assembly/psiclass/clustering",  mode: 'copy'
    label 'process_medium'

    conda "bioconda::vsearch"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/vsearch:2.22.1--hf1761c0_0' :
        'quay.io/biocontainers/vsearch:2.22.1--hf1761c0_0' }"


    input:
    path(psiclasstransdecoderCDS)
    val(clust_id)

    output:
    path "centroids.cds", emit: centroids
    path "centroids.uc", emit: uc
 
    """
    vsearch --notrunclabels --cluster_fast ${psiclasstransdecoderCDS} --id ${clust_id} --centroids centroids.cds --uc centroids.uc 
    """
}

process getComplete_psiclass {
    publishDir "$params.outdir/04_assembly/psiclass/clustering",  mode: 'copy', pattern: '*.cds'
     publishDir "$params.outdir/04_assembly/psiclass/clustering",  mode: 'copy', pattern: '*.txt'
    publishDir "$params.outdir/04_assembly/psiclass",  mode: 'copy', pattern: '*.gff3'
    label 'process_low'  

    conda "bioconda::samtools=1.9"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/samtools:1.9--h91753b0_8' :
        'quay.io/biocontainers/samtools:1.9--h91753b0_8' }"

     
    input:
    path(psiclassCentroids)
    path(gff3)

    output:
    path "*.cds", emit: completeORF
    path "*.txt", emit: completeOutput
    path "model.psiclass.gff3", emit: GeneModel
 
    """
    grep "type:complete" ${psiclassCentroids} | awk '{print \$1}' > id.txt 
sed -i -e 's/>//g' id.txt
while read line;
	do samtools faidx ${psiclassCentroids} \$line >> psiclass.completeORF.cds;
done < id.txt
grep -Fwf id.txt ${gff3} > model.psiclass.gff3

    """
}
