process easel_output {
    label 'process_single'
    publishDir "$params.outdir",  mode: 'copy' 

    input:
    path(genome)
    path(rna_reads)
    path(alignments)
    path(stringtie2)
    path(psiclass)
    path(unfiltered)
    path(filtered)

    output:
    path("easel_summary.txt"), emit: log
      
    """
    cat ${genome} ${rna_reads} ${alignments} ${stringtie2} ${psiclass} ${unfiltered} ${filtered} > easel_summary.txt

    """
}