process ESThints_stringtie2 {
    publishDir "$params.outdir/05_hints/stringtie2",  mode: 'copy'
    label 'process_high'
    container 'cynthiawebster/easel:perl'

    input:
    path(gff)
     
    output:
    path "eggnog.estHints.gff", emit: stringtie2_est
    
    script:
    """
    ${projectDir}/bin/gff2hints.pl --in=${gff} --out=eggnog.estHints.gff
    """
}

process miniprot_stringtie2 {
    label 'process_high'
    
    conda "bioconda::miniprot"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/miniprot:0.9--h7132678_0' :
        'quay.io/biocontainers/miniprot:0.9--h7132678_0' }"
    

    input:
    path(genome)
    path(psiclasstransdecoderPEP)
    val(N)
    val(outn)
    val(max_intron)

    output:
    path "miniprot.gff", emit: stringtie2_miniprot

    script:
    """
    miniprot -N ${N} --outn=${outn} -G ${max_intron} -t ${task.cpus} ${genome} ${psiclasstransdecoderPEP} --gff-only > miniprot.gff
    """
}
process proteinHints_stringtie2 {
    publishDir "$params.outdir/05_hints/stringtie2",  mode: 'copy' 
    label 'process_low'
    container 'cynthiawebster/easel:perl'

    input:
    path(miniprot)

    output:
    path "eggnog.protHints.gff", emit: stringtie2_protein

    script:
    """
    ${projectDir}/bin/align2hints.pl --in=${miniprot} --out=eggnog.protHints.gff --prg=miniprot
    """
}
process gffread_gtf {
    label 'process_low'

    conda "bioconda::gffread=0.12.7"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1' }"

    input:
    path(stringtie2Gmap)
     
    output:
    path "*.gtf", emit: gtf
    
    script:
    """
    gffread ${stringtie2Gmap} -T -o stringtie2.gtf
    """
}
process trainingSet_stringtie2 {
    label 'process_low'
    tag { id }
    
    input:
    path(gtf)
    path(stringtie2Gmap)
    path(genome)
    path(AUGUSTUS_CONFIG_PATH)
    path(script)
    path(bin)
    val(species)
    val(test)
    val(train)
     
    output:
    path "*.gb", emit: gb
    
    script:
    """
    ${script}/computeFlankingRegion.pl ${gtf} | tee calculate_flank.out
    flanking_DNA=\$(grep -A0 "flanking_DNA" calculate_flank.out | awk '{print \$5}')
    echo "\$flanking_DNA" >> calculate_flank.out
    ${script}/gff2gbSmallDNA.pl ${stringtie2Gmap} ${genome} \$flanking_DNA stringtie2Raw.gb
    if [[ -d ${AUGUSTUS_CONFIG_PATH}/species/"$species"_stringtie2 ]]; then rm -rf ${AUGUSTUS_CONFIG_PATH}/species/"$species"_stringtie2; fi
    ${script}/new_species.pl --species="$species"_stringtie2
    ${bin}/etraining --species="$species"_stringtie2 stringtie2Raw.gb 2> failed_genes.txt
    awk '{print \$7}' failed_genes.txt | sed s/://g > badTrainingGenes.lst
    ${script}/filterGenes.pl badTrainingGenes.lst stringtie2Raw.gb > stringtie2GoodGenes.gb
    grep -c "LOCUS" *.gb | tee calculate_flank.out
    ${script}/randomSplit.pl stringtie2GoodGenes.gb ${test}
    ${script}/randomSplit.pl stringtie2GoodGenes.gb.train ${train}
    rm stringtie2GoodGenes.gb.train
    rm stringtie2GoodGenes.gb.train.train
    mv stringtie2GoodGenes.gb.train.test train.gb
    mv stringtie2GoodGenes.gb.test test.gb
    grep -c "LOCUS" *.gb >> calculate_flank.out
    ${bin}/etraining --species="$species"_stringtie2 train.gb
    ${bin}/augustus --species="$species"_stringtie2 | tee stringtie2_untrained.out
    ${script}/optimize_augustus.pl --species="$species"_stringtie2 train.gb --cpus=${task.cpus} --kfold=12 --aug_exec_dir ${AUGUSTUS_CONFIG_PATH}
    ${bin}/etraining --species="$species"_stringtie2 train.gb
    ${bin}/augustus --species="$species"_stringtie2 test.gb | tee stringtie2_optimized.out

    """
}

process augustusEST_stringtie2 {
    label 'process_medium'
    tag { id }

    input:
    path(gb)
    path(ESThints)
    path(script)
    path(configEST)
    path(AUGUSTUS_CONFIG_PATH)
    path(bin)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*stringtie2.ESTHints.gff3", emit: stringtie2_estHints
     
    script: 
    """
    ${bin}/augustus --species="$species"_stringtie2 ${genome_chunk} --extrinsicCfgFile=${configEST} --alternatives-from-evidence=true --hintsfile=${ESThints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_stringtie2.ESTHints.gff3
    """
}

process augustusProtein_stringtie2 {
    label 'process_medium'
    tag { id }

    input:
    path(gb)
    path(proteinHints)
    path(script)
    path(configProtein)
    path(AUGUSTUS_CONFIG_PATH)
    path(bin)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*stringtie2.protHints.gff3", emit: stringtie2_proteinHints
    
    script:  
    """
    ${bin}/augustus --species="$species"_stringtie2 ${genome_chunk} --extrinsicCfgFile=${configProtein} --alternatives-from-evidence=true --hintsfile=${proteinHints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_stringtie2.protHints.gff3

    """
}
process combineEST_stringtie2 {
    publishDir "$params.outdir/06_predictions/stringtie2",  mode: 'copy'
    label 'process_medium'
    tag { id }

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(estHints)
     
    output:
    path("est_stringtie2.gff"), emit: estGFF
      
    """
mkdir est
mv ${estHints} est
for f in est/*.gff3
do
    if wc -l \$f < 25; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

if [ \$(wc -l < files.txt) -eq 1 ]; then
    mv ${estHints} est_stringtie2.gff
else
    files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o est_stringtie2.gff
fi

    """
}

process combineProtein_stringtie2 {
    publishDir "$params.outdir/06_predictions/stringtie2",  mode: 'copy'
    label 'process_medium'
    tag { id }

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(proteinHints)
     
    output:
    path("protein_stringtie2.gff"), emit: stringtie_proteinHints
      
    """
mkdir protein
mv ${proteinHints} protein
for f in protein/*.gff3
do
    if wc -l \$f < 25; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

if [ \$(wc -l < files.txt) -eq 1 ]; then
    mv ${proteinHints} protein_stringtie2.gff
else
    files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o protein_stringtie2.gff
fi
    """
}

process combineAll_stringtie2 {
    publishDir "$params.outdir/06_predictions/stringtie2",  mode: 'copy'
    label 'process_medium'
    tag { id }

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(augustus_protein)
    path(augustus_est)
     
    output:
    path("stringtie2_unfiltered.gff"), emit: stringtie2_combined
      
    """

agat_sp_merge_annotations.pl -f ${augustus_protein} -f ${augustus_est} -o stringtie2_unfiltered.gff

    """
}
