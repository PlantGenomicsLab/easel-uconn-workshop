process entap {
    label 'process_medium'

    container "systemsgenetics/entap:flask"

    input:
    path(entap_config)

    output:
    path('outfiles/databases/entap_database.db'), emit: entap_db
    path('outfiles/databases/eggnog.db'), emit: eggnog_db
    path('outfiles/bin/eggnog_proteins.dmnd'), emit: data_eggnog

    script:
    """
    EnTAP --config \\
        -t ${task.cpus}  \\
        --ini ${entap_config} \\
        --out-dir ./outfiles
    """
}
process orthodb {
    label 'process_medium'

    input:
    val(order)
    val(orthodb)

    output:
    path "*orthodb.fasta", emit: protein

    script:
    """
wget https://data.orthodb.org/download/odb11v0_levels.tab.gz --no-check-certificate
wget https://data.orthodb.org/download/odb11v0_level2species.tab.gz --no-check-certificate

if echo "${order}" | grep -q 'null'
then
    echo "Order not provided, refer to https://www.orthodb.org/"
    exit 1

elif awk -F'\t' '\$1 == "${order}"' ${projectDir}/bin/orthodb.txt 
then
protein=\$(awk -F'\t' '\$1 == "${order}" { print \$2 }' ${projectDir}/bin/orthodb.txt)
wget https://treegenesdb.org/FTP/OrthoDB/v11/"\$protein".fasta.gz --no-check-certificate
gzip -d "\$protein".fasta.gz

GROUP="${order}"
level=`zcat odb11v0_levels.tab.gz | awk -v grp=\$GROUP '\$2 == grp'`
taxid=`echo \$level | cut -d ' ' -f 1`
species=`zcat odb11v0_level2species.tab.gz \\
| awk -v taxid=\$taxid '\$4 ~ "{"\$taxid"}" || 
                       \$4 ~ "{"taxid"," || 
                       \$4 ~ ","taxid"," || 
                       \$4 ~ ","taxid"}" {print \$2}'`
echo \$species | wc -w
echo \$level
regex=`echo \$species | tr ' ' '|'`
grep --no-group-separator -A 1 -P -w "\$regex" "\$protein".fasta > "\$GROUP"_orthodb.fasta
else
echo "Order not found!"
fi 
"""
}
