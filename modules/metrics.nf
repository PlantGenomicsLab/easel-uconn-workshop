process busco_unfiltered {
    publishDir "$params.outdir/metrics/busco",  mode: 'copy', pattern: "unfiltered/*" 
    label 'process_medium'

    conda "bioconda::busco"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/busco:5.4.4--pyhdfd78af_0' :
        'quay.io/biocontainers/busco:5.4.4--pyhdfd78af_0' }"

    input:
    path(protein)
    val(odb)

    output:
    path("unfiltered/*"), emit: busco_unfiltered
    path("unfiltered/*.txt"), emit: busco_unfiltered_txt
    path("busco_complete.txt"), emit: busco_complete
      
    """
    busco -i ${protein} -l ${odb} -o unfiltered -m Protein
    awk 'NR>3 { print \$3 }' unfiltered/*run_*/full_table.tsv > busco_complete.txt
    """
}
process busco_filtered {
    publishDir "$params.outdir/metrics/busco",  mode: 'copy' 
    label 'process_medium'

    conda "bioconda::busco"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/busco:5.4.4--pyhdfd78af_0' :
        'quay.io/biocontainers/busco:5.4.4--pyhdfd78af_0' }"

        
    input:
    path(protein)
    val(odb)

    output:
    path("filtered/*"), emit: busco_filtered
    path("filtered/*.txt"), emit: busco_filtered_txt
      
    """
    busco -i ${protein} -l ${odb}_odb10 -o filtered -m Protein

    """
}
process agat_unfiltered {
    publishDir "$params.outdir/metrics/agat/unfiltered",  mode: 'copy' 
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(gff)
    val(species)

    output:
    path("*statistics.txt"), emit: unfiltered_stats
      
    """
    agat_sp_statistics.pl --gff ${gff} -o ${species}_statistics.txt
    """
}
process agat_filtered {
    publishDir "$params.outdir/metrics/agat/filtered",  mode: 'copy' 
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(gff)
    val(species)

    output:
    path("*statistics.txt"), emit: filtered_stats
      
    """
    agat_sp_statistics.pl --gff ${gff} -o ${species}_statistics.txt
    """
}
process entap_config {
    label 'process_single'
    container "systemsgenetics/entap:flask"

    input:
    val(taxon)
    val(tcoverage)
    val(qcoverage)
    path(entap_db)
    path(eggnog_db)
    path(data_eggnog)
    
    output:
    path("entap_config.ini"), emit: config

"""
entap_db=\$(readlink -f ${entap_db}) 
eggnog_db=\$(readlink -f ${eggnog_db}) 
data_eggnog=\$(readlink -f ${data_eggnog}) 

cat <<EOF >> entap_config.ini
#-------------------------------
# [ini_instructions]
#When using this ini file keep the following in mind:
#	1. Do not edit the input keys to the left side of the '=' sign
#	2. Be sure to use the proper value type (either a string, list, or number)
#	3. Do not add unecessary spaces to your input
#	4. When inputting a list, only add a ',' between each entry
#-------------------------------
# [configuration]
#-------------------------------
#Specify which EnTAP database you would like to download/generate or use throughout execution. Only one is required.
#    0. Serialized Database (default)
#    1. SQLITE Database
#It is advised to use the default Serialized Database as this is fastest.
#type:list (integer)
data-type=1,
#-------------------------------
# [entap]
#-------------------------------
# Path to the EnTAP binary database
# type:string
entap-db-bin=entap_database.bin

# Path to the EnTAP SQL database (not needed if you are using the binary
# database).
# type:string
entap-db-sql=\$entap_db

# Path to the EnTAP graphing script (entap_graphing.py)
# type:string
entap-graph=${projectDir}/bin/entap_graphing.py
#-------------------------------
# [expression_analysis]
#-------------------------------
#Specify the FPKM threshold with expression analysis. EnTAP will filter out transcripts below this value. (default: 0.5)
#type:decimal
fpkm=0.5
#Specify this flag if your BAM/SAM file was generated through single-end reads
#Note: this is only required in expression analysis
#Default: paired-end
#type:boolean (true/false)
single-end=false
#-------------------------------
# [expression_analysis-rsem]
#-------------------------------
# Execution method of RSEM Calculate Expression.
# Example: rsem-calculate-expression
# type:string
rsem-calculate-expression=/usr/local/bin/rsem-calculate-expression

# Execution method of RSEM SAM Validate.
# Example: rsem-sam-validator
# type:string
rsem-sam-validator=/usr/local/bin/rsem-sam-validator

# Execution method of RSEM Prep Reference.
# Example: rsem-prepare-reference
# type:string
rsem-prepare-reference=/usr/local/bin/rsem-prepare-reference

# Execution method of RSEM Convert SAM
# Example: convert-sam-for-rsem
# type:string
convert-sam-for-rsem=/usr/local/bin/convert-sam-for-rsem

#-------------------------------
# [frame_selection]
#-------------------------------
#Select this option if all of your sequences are complete proteins.
#At this point, this option will merely flag the sequences in your output file
#type:boolean (true/false)
complete=false
#Specify the Frame Selection software you would like to use. Only one flag can be specified.
#Specify flags as follows:
#    1. GeneMarkS-T
#    2. Transdecoder (default)
#type:integer
frame-selection=2
#-------------------------------
# [frame_selection-genemarks-t]
#-------------------------------
#Method to execute GeneMarkST. This may be the path to the executable.
#type:string
genemarkst-exe=gmst.pl
#-------------------------------
# [frame_selection-transdecoder]
#-------------------------------
# Method to execute TransDecoder.LongOrfs. This may be the path to the
# executable or simply TransDecoder.LongOrfs.
# type:string
transdecoder-long-exe=/usr/lib/TransDecoder-v5.5.0/TransDecoder.LongOrfs

# Method to execute TransDecoder.Predict. This may be the path to the
# executable or simply TransDecoder.Predict.
# type:string
transdecoder-predict-exe=/usr/lib/TransDecoder-v5.5.0/TransDecoder.Predict

# Transdecoder only. Specify the minimum protein length
# type:integer
transdecoder-m=100

# Specify this flag if you would like to pipe the TransDecoder
# command '--no_refine_starts' when it is executed. Default: False.
# This will 'start refinement identifies potential start codons for 5' partial
# ORFs using a PWM, process on by default.'
# type:boolean (true/false)
transdecoder-no-refine-starts=false

#-------------------------------
# [general]
#-------------------------------
#Specify the output format for the processed alignments.Multiple flags can be specified:
#    1. TSV Format (default)
#    2. CSV Format
#    3. FASTA Amino Acid (default)
#    4. FASTA Nucleotide (default)
#    5. Gene Enrichment Sequence ID vs. Effective Length TSV (default)
#    6. Gene Enrichment Sequence ID vs. GO Term TSV (default)
#type:list (integer)
output-format=1,3,4,5,6,
#-------------------------------
# [ontology]
#-------------------------------
# Specify the ontology software you would like to use
#Note: it is possible to specify more than one! Just usemultiple --ontology flags
#Specify flags as follows:
#    0. EggNOG (default)
#    1. InterProScan
#type:list (integer)
ontology=0,
#Specify the Gene Ontology levels you would like printed
#A level of 0 means that every term will be printed, while a level of 1 or higher
#means that that level and anything higher than it will be printed
#It is possible to specify multiple flags as well
#Example/Defaults: --level 0 --level 1
#type:list (integer)
level=0,1,
#-------------------------------
# [ontology-eggnog]
#-------------------------------
#Path to the EggNOG SQL database that was downloaded during the Configuration stage.
#type:string
eggnog-sql=\$eggnog_db
#Path to EggNOG DIAMOND configured database that was generated during the Configuration stage.
#type:string
eggnog-dmnd=\$data_eggnog
#-------------------------------
# [ontology-interproscan]
#-------------------------------
#Execution method of InterProScan. This is how InterProScan is generally ran on your system.  It could be as simple as 'interproscan.sh' depending on if it is globally installed.
#type:string
interproscan-exe=/usr/local/interproscan/interproscan.sh
#Select which databases you would like for InterProScan. Databases must be one of the following:
#    -tigrfam
#    -sfld
#    -prodom
#    -hamap
#    -pfam
#    -smart
#    -cdd
#    -prositeprofiles
#    -prositepatterns
#    -superfamily
#    -prints
#    -panther
#    -gene3d
#    -pirsf
#    -coils
#    -morbidblite
#Make sure the database is downloaded, EnTAP will not check!
#--protein tigrfam --protein pfam
#type:list (string)
protein=
#-------------------------------
# [similarity_search]
#-------------------------------
#Method to execute DIAMOND. This can be a path to the executable or simply 'diamond' if installed globally.
#type:string
diamond-exe=diamond
#Specify the type of species/taxon you are analyzing and would like alignments closer in taxonomic relevance to be favored (based on NCBI Taxonomic Database)
#Note: replace all spaces with underscores '_'
#type:string
taxon=${taxon}
#Select the minimum query coverage to be allowed during similarity searching
#type:decimal
qcoverage=${qcoverage}
#Select the minimum target coverage to be allowed during similarity searching
#type:decimal
tcoverage=${tcoverage}
#Specify the contaminants you would like to flag for similarity searching. Contaminants can be selected by species or through a specific taxon (insecta) from the NCBI Taxonomy Database. If your taxon is more than one word just replace the spaces with underscores (_).
#Note: since hits are based upon a multitide of factors, a contaminant might end up being the best hit for an alignment. In this scenario, EnTAP will flag the contaminant and it can be removed if you would like.
#type:list (string)
contam=
#Specify the E-Value that will be used as a cutoff during similarity searching.
#type:decimal
e-value=1e-05
#List of keywords that should be used to specify uninformativeness of hits during similarity searching. Generally something along the lines of 'hypothetical' or 'unknown' are used. Each term should be separated by a comma (,) This can be used if you would like to tag certain descriptions or would like to weigh certain alignments differently (see full documentation)
#Example (defaults):
#conserved, predicted, unknown, hypothetical, putative, unidentified, uncultured, uninformative, unnamed
#type:list (string)
uninformative=conserved,predicted,unknown,unnamed,hypothetical,putative,unidentified,uncharacterized,uncultured,uninformative, 
EOF 
    """
}
process entap_unfiltered {
    publishDir "$params.outdir/08_functional_annotation/unfiltered",  mode: 'copy', pattern: 'entap_outfiles/*' 
    label 'process_high'
   
   // currently the only way to run entap
    module '/isg/shared/modulefiles/EnTAP/0.10.8:/isg/shared/modulefiles/anaconda/2.4.0:/isg/shared/modulefiles/perl/5.30.1:/isg/shared/modulefiles/diamond/2.0.6:/isg/shared/modulefiles/interproscan/5.25-64.0:/isg/shared/modulefiles/TransDecoder/5.3.0:/isg/shared/modulefiles/eggnog-mapper/0.99.1'

    input:
    path(protein)
    path(config)
    path(database)

    output:
    path("entap_outfiles/*"), emit: entap_unfiltered
    path("entap_outfiles/log*"), emit: entap_unfiltered_log

   
    """
    if readlink -f "${database}" | grep -q '${projectDir}/bin/dummy.dmnd'
    then
        wget -r -A '*.protein.faa.gz' ftp://ftp.ncbi.nlm.nih.gov/refseq/release/complete/
        gunzip ./ftp.ncbi.nlm.nih.gov/refseq/release/complete/*.gz
        cat ./ftp.ncbi.nlm.nih.gov/refseq/release/plant/*.faa > refseq_complete.protein.faa
        diamond makedb --threads 4 --in refseq_complete.protein.faa -d refseq_complete.protein
        EnTAP --runP --ini ${config} -i ${protein} -d refseq_complete.protein --threads ${task.cpus} --state 4x
        rm -rf ./ftp.ncbi.nlm.nih.gov/
        rm refseq_complete.protein.faa
        rm refseq_complete.protein
    else
        entap_db=\$(readlink -f ${database}) 
        EnTAP --runP --ini ${config} -i ${protein} -d \$entap_db --threads ${task.cpus} --state 4x
    fi 
    """
}
process entap_filtered {
    publishDir "$params.outdir/08_functional_annotation/filtered",  mode: 'copy', pattern: 'entap_outfiles/*' 
    label 'process_high'

    module '/isg/shared/modulefiles/EnTAP/0.10.8:/isg/shared/modulefiles/anaconda/2.4.0:/isg/shared/modulefiles/perl/5.30.1:/isg/shared/modulefiles/diamond/2.0.6:/isg/shared/modulefiles/interproscan/5.25-64.0:/isg/shared/modulefiles/TransDecoder/5.3.0:/isg/shared/modulefiles/eggnog-mapper/0.99.1'
    
    input:
    path(protein)
    path(config)
    path(database)

    output:
    path("entap_outfiles/*"), emit: entap_filtered
    path("entap_outfiles/log*"), emit: entap_filtered_log
      
    """
    if readlink -f "${database}" | grep -q '${projectDir}/bin/dummy.dmnd'
    then
        wget -r -A '*.protein.faa.gz' ftp://ftp.ncbi.nlm.nih.gov/refseq/release/complete/
        gunzip ./ftp.ncbi.nlm.nih.gov/refseq/release/complete/*.gz
        cat ./ftp.ncbi.nlm.nih.gov/refseq/release/plant/*.faa > refseq_complete.protein.faa
        diamond makedb --threads 4 --in refseq_complete.protein.faa -d refseq_complete.protein
        EnTAP --runP --ini ${config} -i ${protein} -d refseq_complete.protein --threads ${task.cpus} --state 4x
        rm -rf ./ftp.ncbi.nlm.nih.gov/
        rm refseq_complete.protein.faa
        rm refseq_complete.protein
    else
        entap_db=\$(readlink -f ${database}) 
        EnTAP --runP --ini ${config} -i ${protein} -d \$entap_db --threads ${task.cpus}
    fi 
    """
}
process unfiltered_output {
    label 'process_single'
    publishDir "$params.outdir/log",  mode: 'copy' 
   
    input:
    path(busco)
    path(gfacs)
    path(entap)
 
    output:
    path("unfiltered.txt"), emit: log
      
    """
    grep -m 1 "Number of gene" ${gfacs} > genes.txt
    grep -m 1 "Number of transcript" ${gfacs} > transcripts.txt
    grep -m 1 "Number of single exon gene" ${gfacs} > mono.txt
    grep "Total unique sequences with an alignment:" ${entap} > aligned.txt
    grep "The lineage" ${busco} > odb.txt
    grep "C:" ${busco} > busco.txt

    alignments=\$(sed 's/.*: //' aligned.txt)
    odb=\$(grep -o '\\b\\w*\\_odb\\w*\\b' odb.txt)
    busco=\$(sed -e 's/[ \t]*//' busco.txt)
    genes=\$(grep -o '[0-9]\\+' genes.txt)
    transcripts=\$(grep -o '[0-9]\\+' transcripts.txt)
    mono=\$(grep -o '[0-9]\\+' mono.txt)

    entap=\$(echo "scale=2 ; \$alignments / \$sequences" | bc) 
    multi=\$((\$genes - \$mono)) 
    mono_multi=\$(echo "scale=2 ; \$mono / \$multi" | bc) 

    echo " " >> unfiltered.txt
    echo "##### Unfiltered #####" >> unfiltered.txt
    echo "Total number of genes: \$genes" >> unfiltered.txt
    echo "Total number of transcripts: \$transcripts" >> unfiltered.txt
    echo "EnTAP alignment rate: \$entap" >> unfiltered.txt
    echo "Mono-exonic/multi-exonic rate: \$mono_multi " >> unfiltered.txt
    echo "BUSCO (\$odb): \$busco" >> unfiltered.txt
    """
}
process filtered_output {
    label 'process_single'
    publishDir "$params.outdir/log",  mode: 'copy' 
    
    input:
    path(busco)
    path(gfacs)
    path(entap)
 
    output:
    path("filtered.txt"), emit: log
      
    """
    grep -m 1 "Number of gene" ${gfacs} > genes.txt
    grep -m 1 "Number of transcript" ${gfacs} > transcripts.txt
    grep -m 1 "Number of single exon gene" ${gfacs} > mono.txt
    grep "Total unique sequences with an alignment:" ${entap} > aligned.txt
    grep "The lineage" ${busco} > odb.txt
    grep "C:" ${busco} > busco.txt

    alignments=\$(sed 's/.*: //' aligned.txt)
    odb=\$(grep -o '\\b\\w*\\_odb\\w*\\b' odb.txt)
    busco=\$(sed -e 's/[ \t]*//' busco.txt)
    genes=\$(grep -o '[0-9]\\+' genes.txt)
    transcripts=\$(grep -o '[0-9]\\+' transcripts.txt)
    mono=\$(grep -o '[0-9]\\+' mono.txt)

    entap=\$(echo "scale=2 ; \$alignments / \$sequences" | bc) 
    multi=\$((\$genes - \$mono)) 
    mono_multi=\$(echo "scale=2 ; \$mono / \$multi" | bc) 

    echo " " >> filtered.txt
    echo "##### Filtered #####" >> filtered.txt
    echo "Total number of genes: \$genes" >> filtered.txt
    echo "Total number of transcripts: \$transcripts" >> filtered.txt
    echo "EnTAP alignment rate: \$entap" >> filtered.txt
    echo "Mono-exonic/multi-exonic rate: \$mono_multi " >> filtered.txt
    echo "BUSCO (\$odb): \$busco" >> filtered.txt
    """
}
