process proteinHints_orthodb {
    publishDir "$params.outdir/05_hints/orthoDB",  mode: 'copy' 
    label 'process_medium'
    container 'cynthiawebster/easel:perl'

    input:
    path(miniprot)

    output:
    path "orthodb.protHints.gff", emit: protein

    script:
    """
    ${projectDir}/bin/align2hints.pl --in=${miniprot} --out=orthodb.protHints.gff --prg=miniprot
    """
}
process augustusProtein_psiclass {
    label 'process_medium'
    tag { id }

    input:
    path(gb)
    path(proteinHints)
    path(script)
    path(configProtein)
    path(AUGUSTUS_CONFIG_PATH)
    path(bin)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*psiclass.orthodb_protHints.gff3", emit: psiclass_proteinHints
      
    """
    ${bin}/augustus --species="$species"_psiclass ${genome_chunk} --extrinsicCfgFile=${configProtein} --alternatives-from-evidence=true --hintsfile=${proteinHints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_psiclass.orthodb_protHints.gff3

    """
}
process augustusProtein_stringtie2 {
    label 'augustus'
    tag { id }


    input:
    path(gb)
    path(proteinHints)
    path(script)
    path(configProtein)
    path(AUGUSTUS_CONFIG_PATH)
    path(bin)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*stringtie2.orthodb_protHints.gff3", emit: stringtie2_proteinHints
    
    script:  
    """
    ${bin}/augustus --species="$species"_stringtie2 ${genome_chunk} --extrinsicCfgFile=${configProtein} --alternatives-from-evidence=true --hintsfile=${proteinHints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_stringtie2.orthodb_protHints.gff3

    """
}
process combineProtein_stringtie2 {
    publishDir "$params.outdir/06_predictions/stringtie2",  mode: 'copy'
    label 'process_medium'
    tag { id }

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"


    input:
    path(proteinHints)
     
    output:
    path("orthodb_protein_stringtie2.gff"), emit: stringtie_proteinHints
      
    """
mkdir protein
mv ${proteinHints} protein
for f in protein/*.gff3
do
    if wc -l \$f < 25; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

if [ \$(wc -l < files.txt) -eq 1 ]; then
    mv ${proteinHints} orthodb_protein_stringtie2.gff
else
    files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o orthodb_protein_stringtie2.gff
fi

    """
}
process combineProtein_psiclass {
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'
    label 'process_medium'
    tag { id }

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"


    input:
    path(proteinHints)
     
    output:
    path("orthodb_protein_psiclass.gff"), emit: psiclass_proteinHints
      
    """
mkdir protein
mv ${proteinHints} protein
for f in protein/*.gff3
do
    if wc -l \$f < 25; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

if [ \$(wc -l < files.txt) -eq 1 ]; then
    mv ${proteinHints} orthodb_protein_psiclass.gff
else
    files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o orthodb_protein_psiclass.gff
fi
    """
}
