process merge_gff {
    label 'process_medium'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"


    input:
    path(stringtie2)
    path(psiclass)
     
    output:
    path("*unfiltered.gff"), emit: unfiltered
    
    """
    agat_sp_merge_annotations.pl -f ${stringtie2} -f ${psiclass} -o merged_unfiltered.gff

    """
}
process fix_exons {
    label 'process_medium'

    conda "bioconda::gffread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1' }"

    input:
    path(gff)
    
     
    output:
    path("fix_exons.gff"), emit: fixed
    
    """
    gffread ${gff} -Z -o fix_exons.gff
    sed -i 's/\tmRNA\t/\ttranscript\t/g' fix_exons.gff
    sed -i 's/geneID/Parent/g' fix_exons.gff
    """
}
process format_gff {
    publishDir "$params.outdir/final_predictions",  mode: 'copy'
    label 'process_medium'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(gff)
    path(genome)
    val(id)
     
    output:
    path("*_unfiltered.gff"), emit: unfiltered_fixed
    
    """
    agat_sp_add_start_and_stop.pl --gff ${gff} --fasta ${genome} --out "$id"_unfiltered.gff

    """
}
process mergeGFF_orthodb {
    label 'process_medium'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"


    input:
    path(stringtie2)
    path(psiclass)
    path(s_ortho)
    path(p_ortho)
     
    output:
    path("*unfiltered.gff"), emit: unfiltered_db
    
    """
    agat_sp_merge_annotations.pl -f ${stringtie2} -f ${psiclass} -f ${s_ortho} -f ${p_ortho} -o merged_unfiltered.gff

    """
}
process list_orthodb {
    label 'process_single'
    
    input:
    path(s_est)
    path(s_prot)
    path(p_est)
    path(p_prot)
    path(p_orthodb)
    path(s_orthodb)

    output:
    path("list.txt"), emit: gff
      
    """
    readlink -f ${s_est} >> list.txt
    readlink -f ${s_prot} >> list.txt
    readlink -f ${p_est} >> list.txt
    readlink -f ${p_prot} >> list.txt
    readlink -f ${p_orthodb} >> list.txt
    readlink -f ${s_orthodb} >> list.txt
    """
}
process list {
    label 'process_single'
    
    input:
    path(s_est)
    path(s_prot)
    path(p_est)
    path(p_prot)

    output:
    path("list.txt"), emit: gff

    """
    readlink -f ${s_est} >> list.txt
    readlink -f ${s_prot} >> list.txt
    readlink -f ${p_est} >> list.txt
    readlink -f ${p_prot} >> list.txt

    """
}
