process ESThints_psiclass {
    publishDir "$params.outdir/05_hints/psiclass",  mode: 'copy'
    label 'process_high'
    container 'cynthiawebster/easel:perl'

    input:
    path(gff)
     
    output:
    path "eggnog.estHints.gff", emit: psiclass_est
    
    script:
    """
    ${projectDir}/bin/gff2hints.pl --in=${gff} --out=eggnog.estHints.gff
    """
}
process miniprot_psiclass {
    label 'process_high'
    
    conda "bioconda::miniprot"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/miniprot:0.9--h7132678_0' :
        'quay.io/biocontainers/miniprot:0.9--h7132678_0' }"
    

    input:
    path(genome)
    path(psiclasstransdecoderPEP)
    val(N)
    val(outn)
    val(max_intron)

    output:
    path "miniprot.gff", emit: psiclass_miniprot

    script:
    """
    miniprot -N ${N} --outn=${outn} -G ${max_intron} -t ${task.cpus} ${genome} ${psiclasstransdecoderPEP} --gff-only > miniprot.gff
    """
}
process proteinHints_psiclass {
    publishDir "$params.outdir/05_hints/psiclass",  mode: 'copy' 
    label 'process_low'
    container 'cynthiawebster/easel:perl'
    

    input:
    path(miniprot)

    output:
    path "eggnog.protHints.gff", emit: psiclass_protein

    script:
    """
    ${projectDir}/bin/align2hints.pl --in=${miniprot} --out=eggnog.protHints.gff --prg=miniprot
    """
}
process gffread_gtf {
    label 'process_low'
    tag { id }

    conda "bioconda::gffread=0.9.12"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.9.12--0' :
        'quay.io/biocontainers/gffread:0.9.12--0' }"

    input:
    path(psiclassGmap)
     
    output:
    path "*.gtf", emit: gtf
      
    """
    gffread ${psiclassGmap} -T -o psiclass.gtf
    """
}
process trainingSet_psiclass {
    label 'process_low'
    tag { id }
    
    input:
    path(gtf)
    path(psiclassGmap)
    path(genome)
    path(AUGUSTUS_CONFIG_PATH)
    path(script)
    path(bin)
    val(species)
    val(test)
    val(train)
     
    output:
    path "*.gb", emit: gb
      
    """
    ${script}/computeFlankingRegion.pl ${gtf} | tee calculate_flank.out
    flanking_DNA=\$(grep -A0 "flanking_DNA" calculate_flank.out | awk '{print \$5}')
    echo "\$flanking_DNA" >> calculate_flank.out
    ${script}/gff2gbSmallDNA.pl ${psiclassGmap} ${genome} \$flanking_DNA psiclassRaw.gb
    if [[ -d ${AUGUSTUS_CONFIG_PATH}/species/"$species"_psiclass ]]; then rm -rf ${AUGUSTUS_CONFIG_PATH}/species/"$species"_psiclass; fi
    ${script}/new_species.pl --species="$species"_psiclass
    ${bin}/etraining --species="$species"_psiclass psiclassRaw.gb 2> failed_genes.txt
    awk '{print \$7}' failed_genes.txt | sed s/://g > badTrainingGenes.lst
    ${script}/filterGenes.pl badTrainingGenes.lst psiclassRaw.gb > psiclassGoodGenes.gb
    grep -c "LOCUS" *.gb | tee calculate_flank.out
    ${script}/randomSplit.pl psiclassGoodGenes.gb ${test}
    ${script}/randomSplit.pl psiclassGoodGenes.gb.train ${train}
    rm psiclassGoodGenes.gb.train
    rm psiclassGoodGenes.gb.train.train
    mv psiclassGoodGenes.gb.train.test train.gb
    mv psiclassGoodGenes.gb.test test.gb
    grep -c "LOCUS" *.gb >> calculate_flank.out
    ${bin}/etraining --species="$species"_psiclass train.gb
    ${bin}/augustus --species="$species"_psiclass | tee psiclass_untrained.out
    ${script}/optimize_augustus.pl --species="$species"_psiclass train.gb --cpus=${task.cpus} --kfold=12 --aug_exec_dir ${AUGUSTUS_CONFIG_PATH}
    ${bin}/etraining --species="$species"_psiclass train.gb
    ${bin}/augustus --species="$species"_psiclass test.gb | tee psiclass_optimized.out

    """
}

process augustusEST_psiclass {
    label 'process_medium'
    tag { id }

    input:
    path(gb)
    path(ESThints)
    path(script)
    path(configEST)
    path(AUGUSTUS_CONFIG_PATH)
    path(bin)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*psiclass.ESTHints.gff3", emit: psiclass_estHints
      
    """
    ${bin}/augustus --species="$species"_psiclass ${genome_chunk} --extrinsicCfgFile=${configEST} --alternatives-from-evidence=true --hintsfile=${ESThints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_psiclass.ESTHints.gff3
    """
}

process augustusProtein_psiclass {
    label 'process_medium'
    tag { id }

    input:
    path(gb)
    path(proteinHints)
    path(script)
    path(configProtein)
    path(AUGUSTUS_CONFIG_PATH)
    path(bin)
    tuple val(id), path(genome_chunk)
    val(species)
     
    output:
    path "*psiclass.protHints.gff3", emit: psiclass_proteinHints
      
    """
    ${bin}/augustus --species="$species"_psiclass ${genome_chunk} --extrinsicCfgFile=${configProtein} --alternatives-from-evidence=true --hintsfile=${proteinHints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_psiclass.protHints.gff3

    """
}

process combineEST_psiclass {
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'
    label 'process_medium'
    tag { id }

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(estHints)
     
    output:
    path("est_psiclass.gff"), emit: estGFF
      
    """
mkdir est
mv ${estHints} est
for f in est/*.gff3
do
    if wc -l \$f < 25; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

if [ \$(wc -l < files.txt) -eq 1 ]; then
    mv ${estHints} est_psiclass.gff
else
    files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o est_psiclass.gff
fi

    """
}

process combineProtein_psiclass {
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'
    label 'process_medium'
    tag { id }

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"

    input:
    path(proteinHints)
     
    output:
    path("protein_psiclass.gff"), emit: psiclass_proteinHints
      
    """

mkdir protein
mv ${proteinHints} protein
for f in protein/*.gff3
do
    if wc -l \$f < 25; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

if [ \$(wc -l < files.txt) -eq 1 ]; then
    mv ${proteinHints} protein_psiclass.gff
else
    files=\$(sed -n 's/..*/&/p' files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o protein_psiclass.gff
fi
    """
}

process combineAll_psiclass {
    publishDir "$params.outdir/06_predictions/psiclass",  mode: 'copy'
    label 'process_medium'
    tag { id }

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
    
    input:
    path(augustus_protein)
    path(augustus_est)
     
    output:
    path("psiclass_unfiltered.gff"), emit: psiclass_combined
      
    """

agat_sp_merge_annotations.pl -f ${augustus_protein} -f ${augustus_est} -o psiclass_unfiltered.gff

    """
}
