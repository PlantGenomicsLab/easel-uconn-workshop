process unfiltered_protein {
    publishDir "$params.outdir/final_predictions",  mode: 'copy', pattern: "*.pep"
    label 'process_medium'
    
    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
    
    input:
    path(genome)
    path(unfiltered_prediction)
    val(prefix)
     
    output: 
    path("*unfiltered.pep"), emit: protein
      
    """
    agat_sp_extract_sequences.pl -g ${unfiltered_prediction} -f ${genome} --protein -o ${prefix}_unfiltered.pep 

    """
}
process eggnog {
    label 'process_medium'
    
    conda "bioconda::eggnog-mapper"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/eggnog-mapper:2.1.9--pyhdfd78af_0' :
        'quay.io/biocontainers/eggnog-mapper:2.1.9--pyhdfd78af_0' }"
    
    input:
    path(protein)
    path(db)

    output: 
    path("eggnogHits.txt"), emit: mapper
      
    """
    emapper.py -i ${protein} -o eggnog --dmnd_db ${db} --override --no_annot -m diamond --cpu $task.cpus
    awk '{print \$1}' eggnog.emapper.hits | sort | uniq > ids.txt 

    awk -F '\t' -v OFS='\t' '{ \$(NF+1) = 1; print }' ids.txt > eggnogHits.txt
    sed -i \$'1 i\\\nTranscript\tHit' eggnogHits.txt
    """
}
process eggnog_feature {
    label 'process_small'
    container 'cynthiawebster/easel:python'
    
    input:
    path(matrix)
    path(eggnog)

    output: 
    path("eggnog.tracking"), emit: eggnog
      
    """
    python ${projectDir}/bin/map_transcript.py ${matrix} ${eggnog} EggNOG feature.tracking
    awk 'BEGIN { FS=OFS="\t" } NR > 1 { \$4 = sprintf("%d", \$4) }; 1' OFS="\t" feature.tracking > eggnog.tracking

    """
}