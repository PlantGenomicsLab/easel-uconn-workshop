process map {
    publishDir "$params.outdir/07_filtering",  mode: 'copy'
    label 'process_single'
     
    input:
    path(gc_ratio)
    path(rna_fold)
    path(gc_content)
    path(length)
    path(weight)
    path(support)
    path(multi_mono)
    path(gene_family)
    path(start_site)
    path(stop_site)
    path(cds_repeat)
    path(orthodb)
    path(expression)
     
    output: 
    path("features.tracking"), emit: features
      
    """
    awk '{print \$4}' ${orthodb} > orthodb.txt
    awk '{print \$4}' ${expression} > expression.txt
    awk '{print \$4}' ${gc_ratio} > gc_ratio.txt
    awk '{print \$4}' ${gc_content} > gc_content.txt
    awk '{print \$4}' ${rna_fold} > rna_fold.txt
    awk '{print \$4}' ${length} > length.txt
    awk '{print \$4}' ${weight} > weight.txt
    awk '{print \$4}' ${multi_mono} > multi_mono.txt
    awk '{print \$4}' ${stop_site} > stop_site.txt
    awk '{print \$4}' ${gene_family} > gene_family.txt
    awk '{print \$4}' ${start_site} > start_site.txt
    awk '{print \$4}' ${cds_repeat} > cds_repeat.txt

    paste ${support} multi_mono.txt gene_family.txt orthodb.txt expression.txt weight.txt length.txt cds_repeat.txt gc_content.txt rna_fold.txt gc_ratio.txt | column -s \$'\t' -t > features.tracking

    """
}
