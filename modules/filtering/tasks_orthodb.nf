process orthodb_bed {
    label 'process_medium'

    conda "bioconda::bedtools"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/bedtools:2.30.0--hc088bd4_0' :
        'quay.io/biocontainers/bedtools:2.30.0--hc088bd4_0' }"


    input:
    path(orthodb_gff)
    path(unfiltered_gff)

    output:
    path "support.txt", emit: bed

    script:
    """
awk '\$3 == "mRNA"' ${orthodb_gff} > miniprot_transcript.gff
awk '\$3 == "transcript"' ${unfiltered_gff} > augustus_transcript.gff
bedtools intersect -a augustus_transcript.gff -b miniprot_transcript.gff -s -f 1.0 -F 1.0 -c | awk '{print \$9, \$10}' | awk -F'[=; ]' -v OFS='\t' '{print \$2,\$5}' > support.txt
sed -i \$'1 i\\\nTranscript\tHit' support.txt

"""
}
process orthodb_feature {
    label 'process_small'
    container 'cynthiawebster/easel:python'

    input:
    path(matrix)
    path(orthodb)

    output:
    path "orthodb.tracking", emit: orthodb

    script:
    """
python ${projectDir}/bin/map_transcript.py ${matrix} ${orthodb} OrthoDB orthodb.tracking

"""
}