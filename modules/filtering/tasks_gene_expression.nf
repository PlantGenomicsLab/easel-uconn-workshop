process htseq {
    label 'process_medium'

    conda "bioconda::htseq"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/htseq:2.0.2--py39h919a90d_0' :
        'quay.io/biocontainers/htseq:2.0.2--py39h919a90d_0' }"

    input:
    tuple val(id), path(bam)
    path(unfiltered_gtf)

    output:
    path "*.counts", emit: counts

    script:
    """
htseq-count -s no -r pos -f bam --idattr transcript_id --type exon ${bam} ${unfiltered_gtf} > ${id}.counts
    """
}
process gene_expression {
    label 'process_medium'
    container 'cynthiawebster/easel:python'

    input:
    path(counts)
    path(matrix)

    output:
    path "expression.tracking", emit: expression

    script:
    """
mkdir read_counts
mv ${counts} read_counts
awk 'NF > 1{ a[\$1] = a[\$1]"\\t"\$2} END {for( I in a ) print I a[I]}' read_counts/*.counts | awk '{sum=0; for (i=2; i<=NF; i++) sum += \$i; print \$1 "\t" sum}' > merged.counts
sed -i \$'1 i\\\nTranscript\tHit\' merged.counts
python ${projectDir}/bin/map_transcript.py ${matrix} merged.counts Expression expression.tracking

    """
}
