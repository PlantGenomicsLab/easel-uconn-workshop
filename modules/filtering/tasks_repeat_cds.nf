process cds_extract {
    label 'process_medium'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
    
     
    input:
    path(genome)
    path(unfiltered_prediction)
    path(matrix)
     
    output: 
    path("CDS.fasta"), emit: cds
      
    """
    awk '{ print \$2 }' ${matrix} > transcripts_list.txt
    agat_sp_extract_sequences.pl -g ${unfiltered_prediction} -f ${genome} -t cds -o CDS.fasta

    """
}

process cds_repeat_content {
    label 'process_low'
    container 'cynthiawebster/easel:python'
     
    input:
    path(cds)
    path(matrix)
     
    output: 
    path("cds_masked.tracking"), emit: cds_repeat
      
    """

    python ${projectDir}/bin/add_percentage_masked_cds.py ${cds}
    
    awk '{ print \$1, \$6}' OFS="\t" masked.tracking > CDS_masked.txt
    sed -i \$'1 i\\\nTranscript\tHit' CDS_masked.txt

    python ${projectDir}/bin/map_transcript.py ${matrix} CDS_masked.txt CDS_Repeat_Content cds_masked.tracking

    """
}
