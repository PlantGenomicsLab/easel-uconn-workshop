process transcript_extract {
    label 'process_medium'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.0.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.0.0--pl5321hdfd78af_0' }"
    

    input:
    path(genome)
    path(unfiltered_prediction)
    path(matrix)

     
    output: 
    path("transcript.fasta"), emit: transcript
      
    """
    awk '{ print \$2 }' ${matrix} > transcript_list.txt
    agat_sp_extract_sequences.pl -g ${unfiltered_prediction} -f ${genome} -t transcript -o transcript.fasta

    """
}

process length {
    label 'process_low'
    container 'cynthiawebster/easel:python'
     
    input:
    path(transcript)
    path(matrix)
     
    output: 
    path("transcript_length.tracking"), emit: length
      
    """
    python ${projectDir}/bin/add_percentage_masked_cds.py ${transcript}
    
    awk '{ print \$1, \$3 }' OFS="\t" masked.tracking > transcript_length.txt
    sed -i \$'1 i\\\nTranscript\tHit' transcript_length.txt

    python ${projectDir}/bin/map_transcript.py ${matrix} transcript_length.txt Length transcript_length.tracking

    """
}

process molecular_weight {
    label 'process_low'
    container 'cynthiawebster/easel:python'
     
    input:
    path(transcript)
    path(matrix)
     
    output: 
    path("transcript_weight.tracking"), emit: weight
      
    """
    python ${projectDir}/bin/add_molecular_weight.py ${transcript}
    
    awk '{ print \$1, \$4 }' OFS="\t" masked.tracking > transcript_weight.txt
    sed -i \$'1 i\\\nTranscript\tHit' transcript_weight.txt

    python ${projectDir}/bin/map_transcript.py ${matrix} transcript_weight.txt Molecular_Weight transcript_weight.tracking

    """
}

process gc {
    label 'process_low'

    conda "bioconda::seqkit"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/seqkit:2.3.1--h9ee0642_0' :
        'quay.io/biocontainers/seqkit:2.3.1--h9ee0642_0' }"
    
    input:
    path(transcript)
     
    output: 
    path("gc_content.txt"), emit: gc
      
    """
    seqkit fx2tab --name --gc ${transcript} > gc.txt 
    awk '{ print \$1, \$5 }' OFS="\t" gc.txt > gc_content.txt
    sed -i \$'1 i\\\nTranscript\tHit' gc_content.txt

    """
}
process gc_python {
    label 'process_low'
    container 'cynthiawebster/easel:python'
     
    input:
    path(matrix)
    path(gc)
     
    output: 
    path("gc_content.tracking"), emit: gc_track
      
    """
    python ${projectDir}/bin/map_transcript.py ${matrix} ${gc} GC_Content gc_content.tracking

    """
}